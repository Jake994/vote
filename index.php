<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Test</title>

    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" rel="stylesheet">  -->

    <!-- Custom styles for this template -->
    <!-- notify -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.3/animate.min.css">


    <!-- Bootstrap core JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script> -->
    <!-- Spinner from font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Notify -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/mouse0270-bootstrap-notify/3.1.5/bootstrap-notify.min.js"></script>


    <style type="text/css">
        [data-notify="progressbar"] {
            margin-bottom: 0px;
            position: absolute;
            bottom: 0px;
            left: 0px;
            width: 100%;
            height: 5px;
        }
    </style>
  </head>

  <body>

    <!-- Page Content -->
    <div class="container">

      <div class="row">


        <div class="col-md-12">
            <div class="login-box well" style="margin-top: 40px;">
                <legend>Vote</legend>
                <div class="form-group text-center">
                    <ul id="vote_list" class="list-group">

                    </ul>
                </div>
            </div>

        </div>

      </div>
      <!-- /.row -->
    </div>
    <!-- <script src="http://localhost:3005/socket.io/socket.io.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.0/socket.io.js"></script>
    <script type="text/javascript">
        var d_name = window.location.hostname;
        var hosting = 'http://'+d_name+':3006';
         var socket = io.connect(hosting);
         var i = 0;
        $(function(){

            // setInterval(function(){
            //      socket.emit('refresh_list');
            //      i++;
            //      console.log(i);
            //  },200)

            socket.emit('refresh_list');

            socket.on('display_list',function(data){
                // console.log(data.length);
                $('.voting').remove('');
                $.each(data,function(index,value){
                    $('#vote_list').append($('<li class="list-group-item voting">').html('<p>'+value.vote_name+'</p>'+'<p>'+value.vote_total+'</p>'+'<button class="voting btn" id='+value.vote_name+' onClick="tester(this.id)">Vote</button>'));
                });
            });

        })

        function tester(event)
        {
            console.log(event);
            socket.emit('submit_vote',{name: event});
        }
    </script>

    <div class="footer navbar-fixed-bottom">
       <footer class="py-5 bg-dark">
          <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; Your Website <?= date('Y')?></p>
          </div>

        </footer>
    </div>

        </body>

    </html>

