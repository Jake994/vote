# STEP TO INSTALL #



### What is this repository for? ###

* Learn how to use socket.io, node.js and chart.js
* Updating chart.js without refreshing chart


### How do I get set up? ###

* Install Node.js from https://nodejs.org/en/
* Create a project folder and use node to the folder
* Run ' npm init' to create package.json and  'npm install'
* Install express 'npm install express --save’
* Install Socket.io ‘npm install socket.io --save’
* Install MySQL ‘npm install mysql --save’

* Install MySQL-events ‘npm install mysql-events --save’
* Refer to https://jinyuwang.weebly.com/for-mysql/how-to-enable-binary-logging-for-mysql
*	This will need to go 'my.ini' at section mysqld add this few line:- 
```
		log-bin=bin.log
		log-bin-index=bin-log.index
		max_binlog_size=100M
		binlog_format=row
		socket=mysql.sock
```

* Install Chart.js 'npm install chart.js --save'

### How to run? ###

* Use Node.js command prompt access to your project folder
* Run 'node vote' to run the server
* Access to 'localhost:4000' to view the result

