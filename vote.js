//Declare mysql and mysql-events
var mysql = require('mysql');
var MySQLEvents = require('mysql-events');

// Declare server and listen to selected socket.
var express = require('express')
  , http = require('http');
var app = express();
var server = http.createServer(app);
var io = require('socket.io').listen(server);

var port = 4000;
server.listen(port, function(){
	console.log('listening on * %d',port);
});


// Read Index file, but not required if used in CodeIgniter
app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});





//MYSQL Create connection with database
var db = mysql.createConnection({
	host:'localhost',
	user:'root',
	password:'',
	database:'nodetest'
})

//This is for Mysqlevent, this function is to help status of selected table
var dsn = {
  host:     'localhost',
  user:     'root',
  password: ''
};

//Declare connection
var myCon = MySQLEvents(dsn);
//Add table and start watcher on the table
var event1 = myCon.add(
  'nodetest.vote',
  function (oldRow, newRow, event) {
     //row inserted 
    if (oldRow === null) {
     	console.log('Insert'); 
		refresh_data()
    }
 
     //row deleted 
    if (newRow === null) {
     	console.log('Delete'); 
		refresh_data()
    }
 
     //row updated 
    if (oldRow !== null && newRow !== null) {
      console.log('Update'); 
      // console.log(newRow);
      refresh_data(); //Refresh data on list
	  update_data(oldRow,newRow);
    }
 
  }, 
  //'match this string or regex'
  'no idea..'
);


function refresh_data(){
	db.query('SELECT * FROM vote', function(err,result,fields){
		if (err) throw err;
		//Broadcast to all user
		io.emit('display_list',result);
	});
}

function update_data(oldRow, newRow){
	db.query('SELECT * FROM vote', function(err,result,fields){
		if (err) throw err;
		//Broadcast to all user
		io.emit('update',result,oldRow,newRow);
	});
}


//Declare connection with front page
//Trigger this when someone connected to this socket
io.on('connection',function(socket){

	console.log(socket.id);

	//This function is trigger from index.html >> 'socket.emit('refresh_list');'
	socket.on('refresh_list',function(data){
		db.query('SELECT * FROM vote', function(err,result,fields){
			if (err) throw err;
			//Broadcast to all user the latest vote list data
			io.emit('display_list',result);
			//Only Broadcast to sender the latest vote list data, to prevent multiple chart printing
			socket.emit('display_chart',result);
		});
	});

	socket.on('submit_vote',function(data){
		db.query("UPDATE vote SET vote_total = vote_total +1 WHERE vote_name = '"+data.name+"'",  function(err,result,fields){
			if (err) throw err;
		});
	});

	
})
